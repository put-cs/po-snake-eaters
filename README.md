# PO - Snake Eaters
Snake Eaters is an iteration on the concept of the classic Snake game.
Core concepts stay the same, but new features are added to make the game fun in a hotseat setting.

This is a project made for the Object-Oriented Programming class at PUT.

# What's new?
Two players battle on an arena, where food spawns around the middle, forcing them to cross paths.
With great length comes great difficulty! Fortunately, players can use special tiles to buy power-ups.

# Licensing
Snake Eaters is licensed under the GNU GPLv3.

# Authors
Michał Miłek & Sebastian Nowak
