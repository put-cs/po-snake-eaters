CC = g++
LIBSFML = -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio -lsfml-network
LIBYAML = -lyaml-cpp
SRC := $(shell find src -name *.cpp)

.PHONY: test clean

snake-eaters: 
	$(CC) -Wall -Wextra -g3 -o snake-eaters $(SRC) $(LIBSFML) $(LIBYAML)

clean:
	rm -f snake-eaters
