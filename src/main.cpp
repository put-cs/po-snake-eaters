#include "Application/Application.h"

int main(void)
{
    Application* app = new Application();
    app->setupWindow();
    app->loadFont();
    app->loop();
    return 0;
}
