#include "BoardEditor.h"
#include "../Application.h"
#include "Button.h"
#include "Menu/Menu.h"
#include "Menu/SettingsMenu/SettingsMenu.h"
#include "Menu/SettingsMenu/TextCarouselEntry.h"
#include <SFML/Graphics/Color.hpp>
#include <SFML/Window/Mouse.hpp>
#include <SFML/Window/Window.hpp>
#include <algorithm>
#include <string>

BoardEditor::BoardEditor(Application* app)
    : SettingsMenu(app)
{
    this->app = app;
    this->buttons_y_position = {
        SCREEN_H - 330,
        SCREEN_H - 260,
        SCREEN_H - 190,
        SCREEN_H - 120,
        SCREEN_H - 50,
    };

    std::vector<std::string> board_names = this->app->game->boards;
    std::vector<std::string> displayed_board_names = this->app->game->boards;

    std::string str2 = "boards/";

    for (auto& name : displayed_board_names) {
        name.erase(0, str2.length());
        name.erase(name.length() - 5, name.length());
    }
    this->entries = {
        TextCarouselEntry("Board", board_names,
            displayed_board_names, &(this->none),
            app, buttons_y_position[0], 1, 0),
        TextCarouselEntry("Fill All", {},
            {}, &(this->fillAll),
            app, buttons_y_position[1], 0, 0),
        TextCarouselEntry("Empty All", {},
            {}, &(this->emptyAll),
            app, buttons_y_position[2], 0, 0),
        TextCarouselEntry("Save", {},
            {}, &(this->saveBoard),
            app, buttons_y_position[3], 0, 0),
        TextCarouselEntry("Return", {},
            {}, &(this->returnToMainMenu),
            app, buttons_y_position[4], 0, 0)
    };
    this->pointer
        .setPosition(this->entries[0].text.getGlobalBounds().left - POINTER_OFFSET,
            buttons_y_position[0]);

    this->applySetting();
}

BoardEditor::~BoardEditor() { }

void BoardEditor::render()
{
    float smaller_sprite_size = 12;
    float padding_h = (SCREEN_W - HORIZONTAL_TILES * smaller_sprite_size) / 2;
    this->app->windowP->clear(sf::Color(40, 40, 40, 255));
    int tile_switcher = 0;
    for (int i = 0; i < HORIZONTAL_TILES; i++) {
        for (int j = 0; j < VERTICAL_TILES; j++) {
            sf::RectangleShape tile;
            tile.setPosition(i * smaller_sprite_size + padding_h, j * smaller_sprite_size + PADDING);
            tile.setSize(sf::Vector2f(smaller_sprite_size, smaller_sprite_size));
            if (tile_switcher % 2 == 0) {
                tile.setFillColor(sf::Color(40, 40, 40));
            } else {
                tile.setFillColor(sf::Color(44, 44, 44));
            }
            tile_switcher++;
            this->app->windowP->draw(tile);
        }
    }

    WallEntity* wall_entity = new WallEntity();
    wall_entity->sprite.setScale(0.75, 0.75);

    for (unsigned long int row = 0; row < this->selected_board.board.size(); row++) {
        for (unsigned long int col = 0; col < this->selected_board.board[row].size(); col++) {
            if (this->selected_board.board[row][col] == TileType::Wall) {
                wall_entity->sprite.setPosition(col * smaller_sprite_size + padding_h, row * smaller_sprite_size + PADDING);
                this->app->windowP->draw(wall_entity->sprite);
            }
        }
    }
    delete wall_entity;

    app->windowP->draw(pointer);
    for (auto button : this->entries) {
        button.render();
    }
}

void BoardEditor::applySetting()
{
    for (long unsigned int i = 0; i < this->entries.size(); i++) {
        if (this->entries[i].focused) {
            TextCarouselEntry* entry = &(this->entries[i]);
            if (entry->debug_text == "Board") {
                this->selected_board = this->app->game->yaml_reader.readBoard(entry->carousel_strings[entry->current_selection]);
            }
            break;
        }
    }
}

void BoardEditor::manageKey(sf::Keyboard::Key key)
{
    switch (key) {
    case sf::Keyboard::Key::Up:
        this->focusUp();
        break;
    case sf::Keyboard::Key::Down:
        this->focusDown();
        break;
    case sf::Keyboard::Key::W:
        this->focusUp();
        break;
    case sf::Keyboard::Key::S:
        this->focusDown();
        break;
    case sf::Keyboard::Left:
        this->scrollLeft();
        this->applySetting();
        break;
    case sf::Keyboard::Right:
        this->scrollRight();
        this->applySetting();
        break;
    case sf::Keyboard::A:
        this->scrollLeft();
        this->applySetting();
        break;
    case sf::Keyboard::D:
        this->scrollRight();
        this->applySetting();
        break;
    case sf::Keyboard::Escape:
        this->goBack(this->app);
        break;
    case sf::Keyboard::Enter:
        this->clickFocused();
        break;
    case sf::Keyboard::E:
        this->clickFocused();
        break;
    default:
        break;
    }
}

void BoardEditor::update()
{
    if (!sf::Mouse::isButtonPressed(sf::Mouse::Left)
        && !sf::Mouse::isButtonPressed(sf::Mouse::Right)) {
        return;
    }

    int x = sf::Mouse::getPosition(*this->app->windowP).x;
    int y = sf::Mouse::getPosition(*this->app->windowP).y;

    float smaller_sprite_size = 12;
    float padding_h = (SCREEN_W - HORIZONTAL_TILES * smaller_sprite_size) / 2;

    // print what tile are we on
    int x_tile = (int)((x - padding_h) / smaller_sprite_size);
    int y_tile = (int)((y - PADDING) / smaller_sprite_size);

    if (x_tile < 0 || y_tile < 0 || y_tile >= VERTICAL_TILES || x_tile >= HORIZONTAL_TILES)
        return;

    TileType selected_type = Wall;

    if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
        selected_type = Wall;
    else if (sf::Mouse::isButtonPressed(sf::Mouse::Right))
        selected_type = Empty;

    this->selected_board.board.at(y_tile).at(x_tile) = selected_type;
}

void BoardEditor::fillAll(Application* app)
{
    for (auto& row : app->board_editor->selected_board.board)
        for (auto& cell : row)
            cell = Wall;
}

void BoardEditor::emptyAll(Application* app)
{
    for (auto& row : app->board_editor->selected_board.board)
        for (auto& cell : row)
            cell = Empty;
}

void BoardEditor::saveBoard(Application* app)
{
    std::string dir = "boards/";
    std::string pre = "custom-board-";
    int counter = 0;
    std::string filename = pre + std::to_string(counter) + ".yaml";

    // generate a unique name
    while (std::any_of(app->game->boards.begin(), app->game->boards.end(), [=](const std::string s) {
        return s == (dir + pre + std::to_string(counter) + ".yaml");
    })) {
        std::cout<<"Found a board named like that\n";
        counter++;
    }
    filename = pre + std::to_string(counter) + ".yaml";
    std::cout<<"Saving to " << dir + filename <<"\n";
    app->game->yaml_reader.saveBoardToFile(&(app->board_editor->selected_board), dir + filename);
    app->game->boards.push_back(dir + filename);
}
