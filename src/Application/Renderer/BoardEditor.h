#ifndef BOARD_EDITOR_H
#define BOARD_EDITOR_H

#include "Button.h"
#include <SFML/Window/Event.hpp>
#include <SFML/Window/Keyboard.hpp>
#include "Menu/SettingsMenu/SettingsMenu.h"
#include "../../Application/Game/Board.h"
#include "../../Application/Game/Game.h"

class BoardEditor : public SettingsMenu {
public:
    Board selected_board = Board(HORIZONTAL_TILES, VERTICAL_TILES, Wall);
    BoardEditor(Application*);
    ~BoardEditor();
    void render();
    void manageKey(sf::Keyboard::Key);
    void applySetting();
    void update();
    static void fillAll(Application*);
    static void emptyAll(Application*);
    static void saveBoard(Application*);
};

#endif
