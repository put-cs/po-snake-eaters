#include "Renderer.h"
#include "../../Application/StateManager.h"
#include "../Application.h"
#include "../Game/Game.h"
#include "Button.h"
#include "Menu/MainMenu.h"
#include <vector>

Renderer::Renderer()
{
    this->render_functions = {
        this->renderMainMenu,
        this->renderSettingsMenu,
        this->renderActualGame,
        this->renderPauseMenu,
        this->renderEndGame,
        this->renderBoardEditor,
        this->renderInfoPage
    };
}

Renderer::~Renderer() { }

void Renderer::render(Application* app, State state)
{
    this->render_functions[state](app);
}

void Renderer::renderMainMenu(Application* app)
{
    app->main_menu->render();
}

void Renderer::renderSettingsMenu(Application* app)
{
    app->settings_menu->render();
}

void Renderer::renderActualGame(Application* app)
{
    app->game->render();
}

void Renderer::renderPauseMenu(Application* app)
{
    app->pause_menu->render();
}

void Renderer::renderEndGame(Application* app) { app->end_menu->render(); }

void Renderer::renderBoardEditor(Application* app)
{
    app->board_editor->render();
}

void Renderer::renderInfoPage(Application* app)
{  
    app->info_page->render();
}
