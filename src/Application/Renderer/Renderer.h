#ifndef RENDERER_H
#define RENDERER_H

#include "../StateManager.h"
#include "../Renderer/Button.h"
#include <SFML/Graphics.hpp>
#include <vector>
class Application;
typedef void (RenderFunction) (Application*);

class Renderer{
public:
    std::vector<RenderFunction*> render_functions;
    void render(Application*,State);
    static void renderMainMenu(Application*);
    static void renderSettingsMenu(Application*);
    static void renderActualGame(Application*);
    static void renderPauseMenu(Application*);
    static void renderEndGame(Application*);
    static void renderBoardEditor(Application*);
    static void renderInfoPage(Application*);
    Renderer();
    ~Renderer();
};

#endif
