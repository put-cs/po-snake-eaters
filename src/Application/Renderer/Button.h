#ifndef BUTTON_H
#define BUTTON_H
#include "../../Application/StateManager.h"
#include <SFML/Graphics.hpp>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Vector2.hpp>
#include <utility>

class Application;

typedef void (onClickFunction)(Application*);

class Button {
public:
    std::string debug_text;
    Application* app;
    sf::Text text;
    onClickFunction* click;
    bool focused;
    Button(std::string, onClickFunction*, Application*, float, bool);
    ~Button();
    void toggleFocus();
    void render();
    void debugPrint();
};

#endif
