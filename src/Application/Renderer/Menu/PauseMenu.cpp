#include "PauseMenu.h"
#include "../../Application.h"
#include "Menu.h"

PauseMenu::PauseMenu(Application* app)
    : Menu(app)
{
    this->app = app;
    this->buttons_y_position = {500,620};
    this->buttons = {
        Button("Continue", &(this->goBack), app, buttons_y_position[0], 1),
        Button("Main Menu", &(this->returnToMainMenu), app, buttons_y_position[1], 0),
    };
    this->pointer.setPosition(buttons[0].text.getGlobalBounds().left - POINTER_OFFSET,buttons_y_position[0]);
}

void PauseMenu::manageKey(sf::Keyboard::Key key)
{
    switch (key) {
    case sf::Keyboard::Key::Up:
        this->focusUp();
        break;
    case sf::Keyboard::Key::Down:
        this->focusDown();
        break;
    case sf::Keyboard::Key::W:
        this->focusUp();
        break;
    case sf::Keyboard::Key::S:
        this->focusDown();
        break;
    case sf::Keyboard::Key::Enter:
        this->clickFocused();
        break;
    case sf::Keyboard::Key::E:
        this->clickFocused();
        break;
    case sf::Keyboard::Key::Escape:
        this->goBack(this->app);
        break;
    default:
        break;
    }
}

void PauseMenu::render(){
    sf::Text t;
    t.setString("PAUSED");
    t.setFont(app->font);
    t.setFillColor(TEXT_COLOR);
    t.setCharacterSize(160);
    sf::FloatRect textRect = t.getLocalBounds();
    t.setOrigin(textRect.left + textRect.width / 2.0f,
        textRect.top + textRect.height / 2.0f);
    t.setPosition(sf::Vector2f(SCREEN_W / 2.0f, 300));
    app->windowP->clear(sf::Color(40,40,40,255));
    app->windowP->draw(t);
    app->windowP->draw(pointer);
    for (auto button : this->buttons) {
        button.render();
    }
}
