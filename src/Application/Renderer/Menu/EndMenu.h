#ifndef END_MENU_H
#define END_MENU_H
#include "Menu.h"
#include <SFML/Window/Keyboard.hpp>

class EndMenu : public Menu {
public:
    EndMenu(Application*);
    ~EndMenu();
    void render();
};
#endif
