#ifndef MENU_H
#define MENU_H

#include "../Button.h"
#include <string>
#include <vector>

class Menu {
public:
    Application* app;
    std::string menu_title;
    std::vector<Button> buttons;
    std::vector<float> buttons_y_position;
    sf::Texture pointer_texture;
    sf::Sprite pointer;
    void manageKey(sf::Keyboard::Key);
    void focusDown();
    void focusUp();
    void clickFocused();
    static void returnToMainMenu(Application* app);
    static void returnToMenuAndStartGame(Application* app);
    static void goBack(Application* app);
    static void startSinglePlayer(Application* app);
    static void startVersusMode(Application* app);
    static void goToBoardEditor(Application* app);
    static void goToSettings(Application* app);
    static void goToInfoPage(Application* app);
    static void exitProgram(Application* app);
    static void none(Application* app);
    Menu(Application*);
    ~Menu();
};

#endif
