#include "MainMenu.h"
#include "../../Application.h"
#include "Menu.h"
#include <cstdlib>

MainMenu::MainMenu(Application* app)
    : Menu(app)
{
    this->title_font.loadFromFile("fonts/Pixeboy.ttf");
    this->app = app;
    this->buttons_y_position = {350, 450, 550, 650, 750, 850};
    this->buttons = {
        Button("Single player", &(this->startSinglePlayer), app, buttons_y_position[0], 1),
        Button("Versus mode", &(this->startVersusMode), app, buttons_y_position[1], 0),
        Button("Board Editor", &(this->goToBoardEditor), app, buttons_y_position[2], 0),
        Button("Settings", &(this->goToSettings), app, buttons_y_position[3], 0),
        Button("Help", &(this->goToInfoPage), app, buttons_y_position[4], 0),
        Button("Exit", &(this->exitProgram), app, buttons_y_position[5], 0)
    };
    this->pointer.setPosition(buttons[0].text.getGlobalBounds().left - POINTER_OFFSET,buttons_y_position[0]);
}

MainMenu::~MainMenu() { }

void MainMenu::render()
{
    sf::Text title;
    title.setFont(this->title_font);
    title.setString("Snake");
    title.setCharacterSize(200);
    title.setLetterSpacing(1.5f);
    sf::FloatRect title_size = title.getLocalBounds();
    title.setOrigin(title_size.left + title_size.width / 2.0f,
                    title_size.top + title_size.height / 2.0f);
    title.setPosition(SCREEN_W / 2.0f, 100);
    sf::Text subtitle;
    subtitle.setFont(this->title_font);
    subtitle.setString("Eaters");
    subtitle.setCharacterSize(150);
    subtitle.setLetterSpacing(1.5f);
    sf::FloatRect subtitle_size = subtitle.getLocalBounds();
    subtitle.setOrigin(subtitle_size.left + subtitle_size.width / 2.0f,
                    subtitle_size.top + subtitle_size.height / 2.0f);
    subtitle.setPosition(SCREEN_W / 2.0f, 220);

    app->windowP->clear(sf::Color(40,40,40,255));
    app->windowP->draw(pointer);
    app->windowP->draw(title);
    app->windowP->draw(subtitle);
    for (auto button : this->buttons) {
        button.render();
    }
}

void MainMenu::debugPrint()
{
    for (auto button : this->buttons) {
        button.debugPrint();
    }
}
