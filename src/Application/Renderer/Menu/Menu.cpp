#include "Menu.h"
#include "../../Application.h"
#include <cstdio>
#include <iostream>

Menu::Menu(Application* app)
{
    this->app = app;
    // app->font.loadFromFile("fonts/CozetteVector.ttf");
    this->pointer_texture.loadFromFile("img/pointer.png");
    this->pointer.setTexture(pointer_texture);
    this->pointer.setScale(2.0f, 2.0f);
    sf::FloatRect size = this->pointer.getLocalBounds();
    this->pointer.setOrigin(size.left + size.width / 2.0f,
        size.top + size.height / 2.0f);
}

Menu::~Menu() { }

void Menu::manageKey(sf::Keyboard::Key key)
{
    switch (key) {
    case sf::Keyboard::Key::Up:
        this->focusUp();
        break;
    case sf::Keyboard::Key::Down:
        this->focusDown();
        break;
    case sf::Keyboard::Key::W:
        this->focusUp();
        break;
    case sf::Keyboard::Key::S:
        this->focusDown();
        break;
    case sf::Keyboard::Key::Enter:
        this->clickFocused();
        break;
    case sf::Keyboard::Key::E:
        this->clickFocused();
        break;
    default:
        break;
    }
}

void Menu::focusDown()
{
    for (long unsigned int i = 0; i < this->buttons.size(); i++) {
        if (this->buttons[i].focused) {
            this->app->audio_player->playSound(CursorMoveClickSound);
            this->buttons[i].toggleFocus();
            this->buttons[(i + 1) % this->buttons.size()].toggleFocus();
            this->pointer.setPosition
                (buttons[(i + 1) % this->buttons.size()].text.getGlobalBounds().left - POINTER_OFFSET,
                 buttons_y_position[(i + 1) % this->buttons.size()]);
            break;
        }
    }
}

void Menu::focusUp()
{
#pragma GCC diagnostic ignored "-Wsign-compare"
    for (long int i = 0; i < this->buttons.size(); i++) {
        if (this->buttons[i].focused) {
            this->app->audio_player->playSound(CursorMoveClickSound);
            this->buttons[i].toggleFocus();
            i--;
            if (i < 0) {
                i = this->buttons.size() - 1;
            }
            this->buttons[i].toggleFocus();
            this->pointer.setPosition(buttons[i].text.getGlobalBounds().left - POINTER_OFFSET, buttons_y_position[i]);
            break;
        }
    }
}

void Menu::clickFocused()
{
    for (long unsigned int i = 0; i < this->buttons.size(); i++) {
        if (this->buttons[i].focused) {
            this->app->audio_player->playSound(CursorMoveClickSound);
            (this->buttons[i].click)(this->app);
            break;
        }
    }
}

void Menu::goBack(Application* app)
{
    app->state_manager->popState();
}

void Menu::returnToMainMenu(Application* app)
{
    while (app->state_manager->state_stack.size() > 1) {
        app->state_manager->popState();
    }
}
void Menu::startSinglePlayer(Application* app)
{
    app->game->single_player = true;
    app->game->init();
    
    app->state_manager->pushState(State::ActualGame);
    app->audio_player->stopMusic(MainMenuMusic);
    app->audio_player->playMusic(GameMusic);
}

void Menu::startVersusMode(Application* app)
{
    app->game->single_player = false;
    app->game->init();
    
    app->state_manager->pushState(State::ActualGame);
    app->audio_player->stopMusic(MainMenuMusic);
    app->audio_player->playMusic(GameMusic);
}

void Menu::goToBoardEditor(Application *app) {
    app->state_manager->pushState(BoardEditor);
}

void Menu::returnToMenuAndStartGame(Application* app)
{
    returnToMainMenu(app);
    if (app->game->single_player)
        startSinglePlayer(app);
    else
        startVersusMode(app);
}

void Menu::goToInfoPage(Application *app) {
    app->state_manager->pushState(InfoPage);
}

#pragma GCC diagnostic ignored "-Wunused-parameter"
void Menu::exitProgram(Application *app) { exit(0); }
void Menu::none(Application *app) {}

void Menu::goToSettings(Application* app) {
    app->state_manager->pushState(Settings);
}
