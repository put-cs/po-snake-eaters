#ifndef MAIN_MENU_H
#define MAIN_MENU_H
#include "Menu.h"

class MainMenu : public Menu {
public:
    sf::Font title_font;
    MainMenu(Application*);
    ~MainMenu();
    void render();
    void debugPrint();
};

#endif
