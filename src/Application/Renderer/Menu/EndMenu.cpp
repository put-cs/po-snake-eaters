#include "EndMenu.h"
#include "Menu.h"
#include "../../Application.h"
#include <SFML/Graphics/Color.hpp>

EndMenu::EndMenu(Application* app) : Menu(app) {
    this->app = app;
    this->buttons_y_position = {420, 540, 660};
    this->buttons = {
        Button("Resume", &(this->returnToMenuAndStartGame), app, buttons_y_position[0], 1),
        Button("Quit to main menu", &(this->returnToMainMenu), app, buttons_y_position[1], 0),
        Button("Exit", &(this->exitProgram), app, buttons_y_position[2], 0),
    };
    this->pointer.setPosition(buttons[0].text.getGlobalBounds().left - POINTER_OFFSET,buttons_y_position[0]);
}
EndMenu::~EndMenu(){}

void EndMenu::render() {
    sf::Text t;
    sf::Text sub_t;

    switch (app->game->lose_state) {
    case LoseState::P1Lost:
        sub_t.setString("Player 1 lost!");
        break;
    case LoseState::P2Lost:
        sub_t.setString("Player 2 lost!");
        break;
    case LoseState::BothLost:
        sub_t.setString("Draw!");
        break;
    default:
        std::cerr<<"An Easter Egg!"<<std::endl;
        break;
    }
    t.setString("Game Over");
    t.setFont(app->font);
    t.setFillColor(TEXT_COLOR);
    t.setCharacterSize(160);
    sf::FloatRect textRect = t.getLocalBounds();
    t.setOrigin(textRect.left + textRect.width / 2.0f,
        textRect.top + textRect.height / 2.0f);
    t.setPosition(sf::Vector2f(SCREEN_W / 2.0f, 150));
    sub_t.setFont(app->font);
    sub_t.setFillColor(TEXT_COLOR);
    sub_t.setCharacterSize(80);
    sf::FloatRect subtextRect = sub_t.getLocalBounds();
    sub_t.setOrigin(subtextRect.left + subtextRect.width / 2.0f,
        subtextRect.top + subtextRect.height / 2.0f);
    sub_t.setPosition(sf::Vector2f(SCREEN_W / 2.0f, 280));
    app->windowP->clear(sf::Color(40,40,40,255));
    app->windowP->draw(this->pointer);
    app->windowP->draw(t);
    app->windowP->draw(sub_t);
    for (auto button : this->buttons) {
        button.render();
    }
}
