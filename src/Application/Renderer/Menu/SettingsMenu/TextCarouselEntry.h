#ifndef TEXT_CAROUSEL_ENTRY_H
#define TEXT_CAROUSEL_ENTRY_H

#include "../../Button.h"
#include <string>
#include <vector>

class TextCarouselEntry : public Button {
public:
    TextCarouselEntry(std::string, std::vector<std::string>, std::vector<std::string>, onClickFunction*, Application*, float, bool, int);
    ~TextCarouselEntry();
    std::vector<std::string> carousel_strings;
    std::vector<std::string> carousel_display_strings;
    int current_selection;
    float y_position;
    void render();
};

#endif
