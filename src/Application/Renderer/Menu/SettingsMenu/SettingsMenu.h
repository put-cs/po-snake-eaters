#ifndef SETTINGS_MENU_H
#define SETTINGS_MENU_H
#include "../Menu.h"
#include "TextCarouselEntry.h"
#include <vector>

class SettingsMenu : public Menu {
public:
    std::vector<TextCarouselEntry> entries;
    SettingsMenu(Application*);
    ~SettingsMenu();
    void render();
    void manageKey(sf::Keyboard::Key);
    void scrollRight();
    void scrollLeft();
    void focusUp();
    void focusDown();
    void clickFocused();
    void applySetting();
};

#endif
