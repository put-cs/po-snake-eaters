#include "SettingsMenu.h"
#include "../../../Application.h"
#include "../Menu.h"
#include "TextCarouselEntry.h"
#include <algorithm>
#include <string>
#include <vector>

SettingsMenu::~SettingsMenu() { }

SettingsMenu::SettingsMenu(Application* app)
    : Menu(app)
{
    this->app = app;
    this->buttons_y_position = { 300, 420, 540, 660 };

    std::vector<std::string> volume_options {
        "0%",
        "10%",
        "20%",
        "30%",
        "40%",
        "50%",
        "60%",
        "70%",
        "80%",
        "90%",
        "100%",
    };

    std::vector<std::string> board_names = this->app->game->boards;
    std::vector<std::string> displayed_board_names = this->app->game->boards;

    std::string str2 = "boards/";

    for (auto& name : displayed_board_names) {
        name.erase(0, str2.length());
        name.erase(name.length() - 5, name.length());
    }

    int initial_sfx_volume = this->app->audio_player->sfx_volume;
    int initial_music_volume = this->app->audio_player->sfx_volume;

    this->entries = {
        TextCarouselEntry("Music Volume", volume_options, volume_options, &(this->none), app, buttons_y_position[0], 1, initial_music_volume / 10),
        TextCarouselEntry("SFX Volume", volume_options, volume_options, &(this->none), app, buttons_y_position[1], 0, initial_sfx_volume / 10),
        TextCarouselEntry("Board", board_names, displayed_board_names, &(this->none), app, buttons_y_position[2], 0, 0),
        TextCarouselEntry("Return", {}, {}, &(this->returnToMainMenu), app, buttons_y_position[3], 0, 0),
    };

    this->pointer.setPosition(entries[0].text.getGlobalBounds().left - POINTER_OFFSET, buttons_y_position[0]);
}

void SettingsMenu::render()
{
    sf::Text t;
    t.setString("Settings");
    t.setFont(app->font);
    t.setFillColor(TEXT_COLOR);
    t.setCharacterSize(160);
    sf::FloatRect textRect = t.getLocalBounds();
    t.setOrigin(textRect.left + textRect.width / 2.0f,
        textRect.top + textRect.height / 2.0f);
    t.setPosition(sf::Vector2f(SCREEN_W / 2.0f, 100));

    this->app->windowP->clear(sf::Color(40, 40, 40, 255));
    this->app->windowP->draw(t);
    for (auto entry : this->entries) {
        entry.render();
    }
    this->app->windowP->draw(this->pointer);
}

void SettingsMenu::manageKey(sf::Keyboard::Key key)
{
    switch (key) {
    case sf::Keyboard::Key::Up:
        this->focusUp();
        break;
    case sf::Keyboard::Key::Down:
        this->focusDown();
        break;
    case sf::Keyboard::Key::W:
        this->focusUp();
        break;
    case sf::Keyboard::Key::S:
        this->focusDown();
        break;
    case sf::Keyboard::Left:
        this->scrollLeft();
        this->applySetting();
        break;
    case sf::Keyboard::Right:
        this->scrollRight();
        this->applySetting();
        break;
    case sf::Keyboard::A:
        this->scrollLeft();
        this->applySetting();
        break;
    case sf::Keyboard::D:
        this->scrollRight();
        this->applySetting();
        break;
    case sf::Keyboard::Escape:
        this->goBack(this->app);
        break;
    case sf::Keyboard::Enter:
        this->clickFocused();
        break;
    case sf::Keyboard::E:
        this->clickFocused();
        break;
    default:
        break;
    }
}

void SettingsMenu::scrollRight()
{
    for (long unsigned int i = 0; i < this->entries.size(); i++) {
        if (this->entries[i].focused) {
            if (this->entries[i].carousel_strings.empty()) {
                return;
            }
            this->app->audio_player->playSound(CursorMoveClickSound);
            this->entries[i].current_selection = (this->entries[i].current_selection + 1) % this->entries[i].carousel_strings.size();
            break;
        }
    }
}

void SettingsMenu::scrollLeft()
{
    for (long unsigned int i = 0; i < this->entries.size(); i++) {
        if (this->entries[i].focused) {
            if (this->entries[i].carousel_strings.empty()) {
                return;
            }
            this->app->audio_player->playSound(CursorMoveClickSound);
            this->entries[i].current_selection--;
            if (this->entries[i].current_selection < 0) {
                this->entries[i].current_selection = this->entries[i].carousel_strings.size() - 1;
            }
            break;
        }
    }
}

void SettingsMenu::focusDown()
{
    for (long unsigned int i = 0; i < this->entries.size(); i++) {
        if (this->entries[i].focused) {

            this->app->audio_player->playSound(CursorMoveClickSound);
            this->entries[i].toggleFocus();
            this->entries[(i + 1) % this->entries.size()].toggleFocus();
            this->pointer.setPosition(entries[(i + 1) % this->entries.size()].text.getGlobalBounds().left - POINTER_OFFSET,
                buttons_y_position[(i + 1) % this->entries.size()]);
            break;
        }
    }
}

void SettingsMenu::focusUp()
{
#pragma GCC diagnostic ignored "-Wsign-compare"
    for (long int i = 0; i < this->entries.size(); i++) {
        if (this->entries[i].focused) {
            this->app->audio_player->playSound(CursorMoveClickSound);
            this->entries[i].toggleFocus();
            i--;
            if (i < 0) {
                i = this->entries.size() - 1;
            }
            this->entries[i].toggleFocus();
            this->pointer.setPosition(entries[i].text.getGlobalBounds().left - POINTER_OFFSET, buttons_y_position[i]);
            break;
        }
    }
}

void SettingsMenu::clickFocused()
{
    for (long unsigned int i = 0; i < this->entries.size(); i++) {
        if (this->entries[i].focused) {
            this->app->audio_player->playSound(CursorMoveClickSound);
            (this->entries[i].click)(this->app);
            break;
        }
    }
}

void SettingsMenu::applySetting()
{
    for (long unsigned int i = 0; i < this->entries.size(); i++) {
        if (this->entries[i].focused) {
            TextCarouselEntry* entry = &(this->entries[i]);
            if (entry->debug_text == "Return") {
                return;
            }
            if (entry->debug_text == "Music Volume") {
                int new_volume = entry->current_selection * 10;
                this->app->audio_player->main_menu.setVolume(new_volume);
                this->app->audio_player->game.setVolume(new_volume);
                this->app->audio_player->end_game.setVolume(new_volume);
                return;
            }
            if (entry->debug_text == "SFX Volume") {
                int new_volume = entry->current_selection * 10;
                this->app->audio_player->sfx_volume = new_volume;
                return;
            }
            if (entry->debug_text == "Board") {
                this->app->game->board = this->app->game->yaml_reader.readBoard(entry->carousel_strings[entry->current_selection]);
            }

            break;
        }
    }
}

