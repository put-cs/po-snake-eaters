#include "TextCarouselEntry.h"
#include "../../../Application.h"
#include <vector>

TextCarouselEntry::TextCarouselEntry(std::string entry_name, std::vector<std::string> options, std::vector<std::string> displayed_options, onClickFunction* click,
    Application* app,
    float y_position,
                                     bool focus, int initial_focus)
    : Button(entry_name, click, app, 4.0, focus)
{
    this->carousel_strings = options;
    this->carousel_display_strings = displayed_options;
    this->current_selection = initial_focus;

    this->app = app;
    this->debug_text = entry_name;
    this->y_position = y_position;

    this->focused = false;
    
    this->text.setFont(app->font);
    this->text.setString(entry_name);
    this->text.setCharacterSize(STANDARD_FONT_SIZE);
    sf::FloatRect text_rect = this->text.getLocalBounds();
    this->text.setOrigin(
        text_rect.left,
        text_rect.top + text_rect.height / 2.0f);
    // align left
    this->text.setPosition(100, this->y_position);
    this->text.setFillColor(TEXT_COLOR);

    if (focus) {
        this->toggleFocus();
    }

    this->click = click;
}

TextCarouselEntry::~TextCarouselEntry() { }

void TextCarouselEntry::render()
{
    this->app->windowP->draw(this->text);
    if (this->carousel_strings.empty())
        return;
    
    sf::Text t;
    t.setCharacterSize(STANDARD_FONT_SIZE);
    t.setFont(this->app->font);
    t.setString(this->carousel_display_strings[this->current_selection]);
    sf::Rect<float> t_rect = t.getLocalBounds();
    t.setOrigin(
        t_rect.left + t_rect.width / 2.0f,
        t_rect.top + t_rect.height / 2.0f);
    t.setPosition(SCREEN_W / 1.4f, this->y_position);
    this->app->windowP->draw(t);

    sf::Sprite arrow;
    sf::Texture arrow_texture;
    arrow_texture.loadFromFile("img/arrow.png");
    arrow.setTexture(arrow_texture);

    const int ARROW_OFFSET = 40;
    // right arrow
    sf::Rect<float> arrow_rect = arrow.getLocalBounds();
    arrow.setOrigin(arrow_rect.left + arrow_rect.width/2.0, arrow_rect.top + arrow_rect.height / 2.0);
    arrow.setPosition(t.getGlobalBounds().left + t.getGlobalBounds().width + ARROW_OFFSET, this->y_position);
    this->app->windowP->draw(arrow);


    // left arrow
    arrow.setRotation(180);
    arrow.setPosition(t.getGlobalBounds().left - ARROW_OFFSET, this->y_position);
    this->app->windowP->draw(arrow); 
    
    
}
