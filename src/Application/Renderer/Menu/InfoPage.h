#ifndef INFO_PAGE_H
#define INFO_PAGE_H
#include "Menu.h"

class InfoPage : public Menu {
public:
    InfoPage(Application*);
    ~InfoPage();
    void render();
};

#endif
