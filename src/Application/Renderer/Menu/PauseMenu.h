#ifndef PAUSE_MENU_H
#define PAUSE_MENU_H

#include "Menu.h"

class PauseMenu : public Menu {
public:
    PauseMenu(Application*);
    ~PauseMenu();
    void manageKey(sf::Keyboard::Key key);
    void render();
    void debugPrint();
};

#endif
