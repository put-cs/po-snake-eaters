#include "InfoPage.h"
#include "../../Application.h"
#include "Menu.h"
#include <cstdlib>

InfoPage::InfoPage(Application* app)
    : Menu(app)
{
    this->app = app;
    this->buttons_y_position = {830};
    this->buttons = {
        Button("Return", &(this->goBack), app, buttons_y_position[0], 1),
    };
    this->pointer.setPosition(buttons[0].text.getGlobalBounds().left - POINTER_OFFSET,buttons_y_position[0]);
}

InfoPage::~InfoPage() {}

void InfoPage::render() {
    std::vector<sf::Text> texts(5);
    for (auto& text: texts){
        text.setFont(this->app->font);
        text.setCharacterSize(35);
    }
    texts[0].setString("W S A D - P1 movement");
    texts[1].setString("Up Down Left Right - P2 movement");
    texts[2].setString("E, Enter - Select");
    texts[3].setString("Speed Up, Slow Down, Super Fruit, Extra Foods - Powerups");
    texts[4].setString("LMB, RMB - Place, remove walls in the Board Editor");

    this->app->windowP->clear(sf::Color(40,40,40,255));
    for (long unsigned int i=0; i < texts.size();i++){
        texts[i].setPosition(15, 30 + i*70);
        this->app->windowP->draw(texts[i]);
    }
    this->app->windowP->draw(pointer);
    buttons[0].render();
}
