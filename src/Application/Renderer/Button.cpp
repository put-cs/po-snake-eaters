#include "Button.h"
#include "../../Application/Application.h"
#include "../../Application/StateManager.h"
#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/System/Vector2.hpp>
#include <utility>
#include <vector>

Button::Button(std::string text,
    onClickFunction* click,
    Application* app,
    float y_position,
    bool focused)
{
    this->app = app;

    this->focused = false;
    this->debug_text = text;
    this->text.setFont(app->font);
    app->font.loadFromFile("fonts/CozetteVector.ttf");
    this->text.setString(text);
    this->text.setCharacterSize(STANDARD_FONT_SIZE);
    sf::FloatRect text_rect = this->text.getLocalBounds();
    this->text.setOrigin(
        text_rect.left + text_rect.width / 2.0f,
        text_rect.top + text_rect.height / 2.0f);
    this->text.setPosition(SCREEN_W/ 2.0f, y_position);
    this->text.setFillColor(TEXT_COLOR);
    
    if (focused) {
        this->toggleFocus();
    }
    
    this->click = click;
}

Button::~Button() { }

void Button::toggleFocus()
{
    if (this->focused) {
        this->focused = false;
        this->text.setFillColor(TEXT_COLOR);
    } else {
        this->focused = true;
        this->text.setFillColor(sf::Color(200,200,200,255));
    }
}

void Button::render()
{
    this->app->windowP->draw(this->text);
}

void Button::debugPrint() {
    std::cout<<"Button "<<this->debug_text<<"\n";
    std::cout<<"Focused?: "<<this->focused<<"\n";
}
