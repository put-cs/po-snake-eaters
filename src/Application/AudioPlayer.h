#ifndef AUDIO_PLAYER_H
#define AUDIO_PLAYER_H

#include "SFML/Audio.hpp"
#include <SFML/Audio/Music.hpp>
#include <SFML/Audio/Sound.hpp>
#include <SFML/Audio/SoundBuffer.hpp>
#include <deque>
#include <map>
#include <vector>

enum MusicName {
    MainMenuMusic,
    GameMusic,
    EndGameMusic
};

enum SoundName {
    LoseSound,
    EatSound,
    CursorMoveClickSound
};

class AudioPlayer {
public:
    AudioPlayer();
    ~AudioPlayer();
    sf::Music main_menu;
    sf::Music game;
    sf::Music end_game;
    int sfx_volume = 100;
    sf::SoundBuffer click_sound_buffer;
    sf::SoundBuffer eat_sound_buffer;
    sf::SoundBuffer lose_sound_buffer;
    std::deque<sf::Sound> sound_queue = {};
    void pauseAllMusicAndPlay(MusicName);
    void stopAllMusicAndPlay(MusicName name);
    void ensurePlaying(MusicName);
    void playMusic(MusicName);
    void pauseMusic(MusicName);
    void stopMusic(MusicName);
    void playSound(SoundName);
    void collectGarbage();
};

#endif
