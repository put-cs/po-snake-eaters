#include "StateManager.h"
#include <iostream>

StateManager::StateManager(){
    this->state_stack = {};
}

StateManager::~StateManager(){
}

void StateManager::pushState(State state){
    this->state_stack.push_back(state);
}

void StateManager::popState(){
    this->state_stack.pop_back();
}

State StateManager::getCurrentState() {
    return this->state_stack.at(state_stack.size()-1);
}

void StateManager::printStack(){
    for (auto& state : this->state_stack){
    std::cerr<<state<<" ";
    }
    std::cerr<<"\n";
}