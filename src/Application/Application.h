#ifndef APPLICATION_H
#define APPLICATION_H

#include "AudioPlayer.h"
#include "Renderer/BoardEditor.h"
#include "Renderer/Menu/EndMenu.h"
#include "Renderer/Menu/MainMenu.h"
#include "Renderer/Menu/PauseMenu.h"
#include "Renderer/Menu/SettingsMenu/SettingsMenu.h"
#include "Renderer/Menu/InfoPage.h"
#include "Renderer/Renderer.h"
#include "StateManager.h"
#include "Game/YamlReader.h"
#include "Game/Game.h"
#include "Renderer/BoardEditor.h"
#include <deque>

class Application {
public:
    Renderer* renderer;
    Game* game;
    class MainMenu* main_menu;
    class PauseMenu* pause_menu;
    class InfoPage* info_page;
    EndMenu* end_menu;
    SettingsMenu* settings_menu;
    StateManager* state_manager;
    YamlReader* yaml_reader;
    AudioPlayer* audio_player;
    sf::RenderWindow* windowP;
    class BoardEditor* board_editor;
    sf::Font font;
    void render();
    Application();
    void manageKey(sf::Event::KeyEvent event);
    void setupWindow();
    void loadFont();
    void loop();
    ~Application();
};

#endif
