#ifndef STATEMANAGER_H
#define STATEMANAGER_H
#include <SFML/Graphics.hpp>
#include <vector>

enum State{
    MainMenu,
    Settings,
    ActualGame,
    PauseMenu,
    EndGame,
    BoardEditor,
    InfoPage,
};

class StateManager{
public:
    std::vector<State> state_stack;
    void pushState(State);
    void popState();
    void printStack();
    State getCurrentState();
    StateManager();
    ~StateManager();
};

#endif
