#include "Application.h"
#include "AudioPlayer.h"
#include "Renderer/BoardEditor.h"
#include "Renderer/Menu/EndMenu.h"
#include "Renderer/Menu/InfoPage.h"
#include "Renderer/Menu/SettingsMenu/SettingsMenu.h"
#include "Renderer/Renderer.h"
#include "StateManager.h"
#include <iostream>

void Application::render()
{
    renderer->render(this, this->state_manager->getCurrentState());
    this->windowP->display();
}

Application::~Application() { delete this->windowP; }

void Application::setupWindow()
{
    this->windowP->setFramerateLimit(60);
    this->windowP->setKeyRepeatEnabled(false);
}

void Application::manageKey(sf::Event::KeyEvent key_event)
{
    switch (this->state_manager->getCurrentState()) {
    case State::MainMenu:
        this->main_menu->manageKey(key_event.code);
        this->audio_player->stopMusic(EndGameMusic);
        break;
    case State::Settings:
        this->settings_menu->manageKey(key_event.code);
        break;
    case State::ActualGame:
        this->game->manageKey(key_event.code);
        break;
    case State::PauseMenu:
        this->pause_menu->manageKey(key_event.code);
        break;
    case State::EndGame:
        this->end_menu->manageKey(key_event.code);
        break;
    case State::BoardEditor:
        this->board_editor->manageKey(key_event.code);
        break;
    case State::InfoPage:
        this->info_page->manageKey(key_event.code);
        break;  
    default:
        std::cout<<"No such state!\n";
        break;
    }
}

Application::Application()
{
    this->game = new Game(this);
    this->main_menu = new class MainMenu(this);
    this->pause_menu = new class PauseMenu(this);
    this->info_page = new class InfoPage(this);
    this->end_menu = new EndMenu(this);
    this->audio_player = new AudioPlayer();
    this->windowP = new sf::RenderWindow(sf::VideoMode(SCREEN_W, SCREEN_H),
        "snake-eaters",
        sf::Style::Default);
    this->renderer = new Renderer();
    this->state_manager = new StateManager();
    this->font = sf::Font();
    this->settings_menu = new SettingsMenu(this);
    this->board_editor = new class BoardEditor(this);
    this->state_manager->pushState(State::MainMenu);
    this->audio_player->playMusic(MainMenuMusic);
}

void Application::loadFont()
{
    if (!this->font.loadFromFile("fonts/CozetteVector.ttf")) {
        puts("Failed to load font");
    }
}

void Application::loop()
{
    while (this->windowP->isOpen()) {
        sf::Event evnt;
        while (this->windowP->pollEvent(evnt)) {
            if (evnt.type == evnt.Closed) {
                this->windowP->close();
            }
            if (evnt.type == evnt.KeyPressed) {
                this->manageKey(evnt.key);
            }
        }
        switch (this->state_manager->getCurrentState()) {
        case ActualGame:
            game->loop();
            break;
        case MainMenu:
            this->audio_player->ensurePlaying(MainMenuMusic);
            break;
        case Settings:
            break;
        case PauseMenu:
            break;
        case EndGame:
            break;
        case BoardEditor:
            this->board_editor->update();
            break;
        case InfoPage:
            break;
        default:
            std::cout << "No such state, perhaps the state stack is empty?\n";
            break;
        }
        this->audio_player->collectGarbage();
        this->render();
    }
}
