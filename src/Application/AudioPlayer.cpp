#include "AudioPlayer.h"
#include "Game/Game.h"
#include "Renderer/Menu/MainMenu.h"
#include <SFML/Audio/Music.hpp>
#include <SFML/Audio/SoundBuffer.hpp>
#include <algorithm>
#include <utility>

AudioPlayer::~AudioPlayer() { }

AudioPlayer::AudioPlayer()
{
    this->main_menu.openFromFile("audio/snake-eater-8bit.ogg");
    this->main_menu.setLoop(true);

    this->game.openFromFile("audio/ff4-theme-extended.ogg");
    this->game.setLoop(true);

    this->end_game.openFromFile("audio/resident-1-safehouse-8bit.ogg");
    this->end_game.setLoop(true);

    // this->cursor_move_or_select.buffer.loadFromFile("audio/cursor-move-select.ogg");
    // this->cursor_move_or_select.sound.setBuffer(this->cursor_move_or_select.buffer);

    this->click_sound_buffer.loadFromFile("audio/cursor-move-select.ogg");
}

void AudioPlayer::playMusic(MusicName name)
{
    switch (name) {
    case MainMenuMusic:
        this->main_menu.play();
        break;
    case GameMusic:
        this->game.play();
        break;
    case EndGameMusic:
        this->end_game.play();
        break;
    }
}

void AudioPlayer::playSound(SoundName name)
{
    sf::Sound x(this->lose_sound_buffer);
    x.setVolume(this->sfx_volume);
    sf::Sound y(this->eat_sound_buffer);
    y.setVolume(this->sfx_volume);
    sf::Sound z(this->click_sound_buffer);
    z.setVolume(this->sfx_volume);

    switch (name) {
    case LoseSound:
        this->sound_queue.push_back(x);
        this->sound_queue.back().play();
        break;
    case EatSound:
        this->sound_queue.push_back(y);
        this->sound_queue.back().play();
        break;
    case CursorMoveClickSound:
        this->sound_queue.push_back(z);
        this->sound_queue.back().play();
        break;
    }
}

void AudioPlayer::pauseAllMusicAndPlay(MusicName name)
{
    main_menu.pause();
    game.pause();
    end_game.pause();
    this->playMusic(name);
}

void AudioPlayer::stopAllMusicAndPlay(MusicName name)
{
    main_menu.stop();
    game.stop();
    end_game.stop();
    this->playMusic(name);
}

void AudioPlayer::ensurePlaying(MusicName name)
{
    switch (name) {
    case MainMenuMusic:
        if (this->main_menu.getStatus() != sf::Music::Status::Playing) {
            this->pauseAllMusicAndPlay(MainMenuMusic);
        }
        break;
    case GameMusic:
        if (this->game.getStatus() != sf::Music::Status::Playing) {
            this->pauseAllMusicAndPlay(GameMusic);
        }
        break;
    case EndGameMusic:
        if (this->end_game.getStatus() != sf::Music::Status::Playing) {
            this->pauseAllMusicAndPlay(EndGameMusic);
        }
        break;
    }
}

void AudioPlayer::collectGarbage()
{
    if (this->sound_queue.empty()) {
        return;
    }
    while (this->sound_queue.front().getStatus() != sf::SoundSource::Playing) {
        this->sound_queue.pop_front();
        if (this->sound_queue.empty()) {
            return;
        }
    }
}

void AudioPlayer::pauseMusic(MusicName name)
{
    switch (name) {
    case MainMenuMusic:
        this->main_menu.pause();
        break;
    case GameMusic:
        this->game.pause();
        break;
    case EndGameMusic:
        this->end_game.pause();
        break;
    }
}

void AudioPlayer::stopMusic(MusicName name)
{
    switch (name) {
    case MainMenuMusic:
        this->main_menu.stop();
        break;
    case GameMusic:
        this->game.stop();
        break;
    case EndGameMusic:
        this->end_game.stop();
        break;
    }
}
