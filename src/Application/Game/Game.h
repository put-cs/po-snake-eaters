#ifndef GAME_H
#define GAME_H

#include "../Renderer/Button.h"
#include "../Renderer/Renderer.h"
#include "Board.h"
#include "Entity/Entity.h"
#include "Entity/Food.h"
#include "Entity/Powerup.h"
#include "Entity/Snake.h"
#include "Entity/Tile.h"
#include "Entity/Wall.h"
#include "../StateManager.h"
#include "YamlReader.h"
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <deque>
#include <utility>
#include <vector>
#include <iomanip>
#include <iostream>

#define SCREEN_W 1044
#define SCREEN_H 900
#define SPRITE_SIZE 16
#define PADDING 10
#define TARGET_FOOD 15
#define TARGET_POWERUP 2
#define MILIS_BETWEEN_UPDATES 100
#define HORIZONTAL_TILES 64
#define VERTICAL_TILES 45
#define STANDARD_FONT_SIZE 50
#define POINTER_OFFSET 40
#define TEXT_COLOR sf::Color(255,255,255,255)

enum LoseState {
    None,
    P1Lost,
    P2Lost,
    BothLost
};

class Game {
public:
    bool single_player = false;
    Application* application;
    Board board;
    std::vector<int> powerup_counter;
    std::vector<int> powerup_modifiers;
    int powerup_id;
    std::vector<Snake*> players;
    sf::Clock clock;
    sf::Time dt;
    std::vector<Food> food_vec;
    std::vector<Powerup> powerup_vec;
    std::vector<std::string> boards;
    YamlReader yaml_reader;
    int lose_state = LoseState::None;
    void updateDt();
    void render();
    void manageKey(sf::Keyboard::Key);
    void maybeSpawnFood();
    void maybeSpawnPowerUp();
    void powerupCountDown();
    void maybePowerupReset();
    std::string getLatestPowerup(int);
    int getFoodCount();
    int getPowerupCount();
    void updateKnownBoards();
    void init();
    void loop();
    Game(Application*);
    ~Game();
};

#endif
