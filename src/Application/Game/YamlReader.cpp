#include "YamlReader.h"
#include "Board.h"
#include "Game.h"
#include "chrono"
#include "yaml-cpp/yaml.h"
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sys/types.h>
#include <yaml-cpp/emitter.h>
#include <yaml-cpp/emittermanip.h>
#include <yaml-cpp/node/detail/iterator_fwd.h>
#include <yaml-cpp/node/node.h>
#include <yaml-cpp/node/parse.h>

Board YamlReader::readBoard(std::string path)
{
    YAML::Node yaml_file = YAML::LoadFile(path);
    Board board(HORIZONTAL_TILES, VERTICAL_TILES, Empty);

    for (size_t i = 0; i < yaml_file["points"].size(); i++) {
        int x = yaml_file["points"][i][0].as<int>();
        int y = yaml_file["points"][i][1].as<int>();
        //board.board[VERTICAL_TILES - y - 1][x] = TileType::Wall;
        board.board[y][x] = TileType::Wall;
    }

    return board;
}

// Get current date/time, format is YYYY-MM-DD.HH:mm:ss
const std::string currentDateTime()
{
    time_t now = time(0);
    struct tm tstruct;
    char buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%Y-%m-%d-%X", &tstruct);

    return buf;
}

void YamlReader::saveBoardToFile(Board* board, std::string filename)
{
    YAML::Emitter out;
    out << YAML::BeginMap << "points";
    out << YAML::Flow;
    out << YAML::BeginSeq;
    
    for (long unsigned int i = 0; i < board->board.size(); i++)
        for (long unsigned int j = 0; j < board->board[i].size(); j++)
            if (board->board[i][j] == Wall) {
                out << YAML::Flow;
                out << YAML::BeginSeq << j << i << YAML::EndSeq;
            }
    
    out << YAML::EndSeq;
    out << YAML::EndMap;

    std::ofstream out_file(filename);

    out_file << out.c_str();

    out_file.close();
}
