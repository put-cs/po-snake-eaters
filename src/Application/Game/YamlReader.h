#ifndef YAML_READER_H
#define YAML_READER_H

#include "Board.h"
#include <string>

class YamlReader {
 public:
    Board readBoard(std::string);
    void saveBoardToFile(Board*, std::string);
};

#endif
