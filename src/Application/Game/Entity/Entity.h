#ifndef ENTITY_H_
#define ENTITY_H_
#include <SFML/Graphics.hpp>

class Entity {
public:
    sf::Texture texture;
    sf::Sprite sprite;
    float x;
    float y;

    Entity();
    virtual ~Entity();
};
#endif // ENTITY_H_
