#include "Powerup.h"
#include "../../Application.h"
#include <random>

Powerup::Powerup() {
    this->texture.loadFromFile("img/powerup.png");
    this->sprite.setTexture(texture);
};

Powerup::~Powerup(){};

void Powerup::speedUp(Game* game){
    game->powerup_counter[SpeedUp] += 70;
    game->powerup_modifiers[SpeedUp] = 40;
};

void Powerup::slowDown(Game* game){
    game->powerup_counter[SlowDown] += 30;
    game->powerup_modifiers[SlowDown] = 40;
}

void Powerup::extraFoods(Game * game){
    game->powerup_counter[ExtraFoods] += 3;
    game->powerup_modifiers[ExtraFoods] = 3;
}

void Powerup::superFood(Game *game){
    game->powerup_counter[SuperFood] += 7;
    game->powerup_modifiers[SuperFood] = 1;
}

int Powerup::randomPowerUp(Game* game){
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<> rand_x(0, 3);
    int random_powerup = rand_x(gen);
    switch (random_powerup) {
    case 0:
        this->speedUp(game);
        break;
    case 1:
        this->slowDown(game);
        break;
    case 2:
        this->extraFoods(game);
        break;
    case 3:
        this->superFood(game);
        break;
    default:
        std::cerr<<"Random seeding failed."<<std::endl;
        break;
    }
    return random_powerup;
}