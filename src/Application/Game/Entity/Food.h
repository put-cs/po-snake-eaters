#ifndef FOOD_H_
#define FOOD_H_

#include "Entity.h"
#include <SFML/Graphics.hpp>

class Food : public Entity {
public:
    Food();
    ~Food();
};

#endif
