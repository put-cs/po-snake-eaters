#include "Snake.h"
#include "../Game.h"
#include "Food.h"
#include "Powerup.h"
#include <algorithm>
#include <cstdio>
#include <deque>
#include <string>
#include <vector>

void Snake::spawn(float x, float y)
{
    this->x = x;
    this->y = y;
}

bool Snake::canMove()
{
    Point next_pos = Point(0, 0);
    Point curr_head = this->snake_queue->back();
    if (this->dir == Direction::Up) {
        next_pos = Point(curr_head.x, curr_head.y - 1);
    } else if (this->dir == Direction::Right) {
        next_pos = Point(curr_head.x + 1, curr_head.y);
    } else if (this->dir == Direction::Down) {
        next_pos = Point(curr_head.x, curr_head.y + 1);
    } else {
        next_pos = Point(curr_head.x - 1, curr_head.y);
    }

    // if collided with wall
    if ((next_pos.x < 0 || next_pos.x >= HORIZONTAL_TILES) || (next_pos.y < 0 || next_pos.y >= VERTICAL_TILES)) {
        return false;
    }

    // collision with self
    for (auto it = this->snake_queue->cbegin(); it != this->snake_queue->cend() - 1; ++it) {
        if (it->x == next_pos.x && it->y == next_pos.y) {
            return false;
        }
    }

    if (this->game->board.board[next_pos.y][next_pos.x] == TileType::Wall) {
        return false;
    }

    // if collided with another snake
    for (auto& snake : this->game->players) {
        if (snake->id == this->id) {
            continue;
        }
        // other snake's snake queue contains next pos
        if (std::any_of(snake->snake_queue->begin(), snake->snake_queue->end(), [=](const Point pos) {
                return (pos.x == next_pos.x && pos.y == next_pos.y);
            })) {
            return false;
        }
    }
    return true;
}

void Snake::move()
{
    using Direction = Snake::Direction;
    std::deque<Point>* q = this->snake_queue;
    Point curr_head = q->back();
    if (this->dir == Direction::Up) {
        q->push_back(Point(curr_head.x, curr_head.y - 1));
        this->last_move = Up;
    } else if (this->dir == Direction::Right) {
        q->push_back(Point(curr_head.x + 1, curr_head.y));
        this->last_move = Right;
    } else if (this->dir == Direction::Down) {
        q->push_back(Point(curr_head.x, curr_head.y + 1));
        this->last_move = Down;
    } else {
        this->last_move = Left;
        q->push_back(Point(curr_head.x - 1, curr_head.y));
    }
    this->maybeGrow();
    this->consumeThyPowerup();
    this->x = this->snake_queue->back().x;
    this->y = this->snake_queue->back().y;
}

void Snake::maybeGrow()
{
    // if the snake didn't collide with food, pop the queue
    std::vector<Food> v = this->game->food_vec;
    std::vector<Food> v_new;
    int collision_idx = -1;
    for (unsigned long int i = 0; i < this->game->food_vec.size(); i++) {
        if (this->x == (v[i].x - PADDING) / SPRITE_SIZE && this->y == (v[i].y - PADDING) / SPRITE_SIZE) {
            collision_idx = i;
        }
    }
    #pragma GCC diagnostic ignored "-Wsign-compare"
    if (collision_idx == -1) {
        if (this->game->powerup_counter[SuperFood] == 0){
            this->snake_queue->pop_front();
        }
        return;
    }
    for (long unsigned int i = 0; i < this->game->food_vec.size(); i++) {
        if (collision_idx != i) {
            v_new.push_back(v[i]);
        }
    }
    this->game->food_vec = v_new;
}

void Snake::consumeThyPowerup(){
    std::vector<Powerup> p = this->game->powerup_vec;
    std::vector<Powerup> p_new;
    int collision_idx = -1;
    for (unsigned long int i = 0; i < this->game->powerup_vec.size(); i++) {
        if (this->x == (p[i].x - PADDING) / SPRITE_SIZE && this->y == (p[i].y - PADDING) / SPRITE_SIZE) {
            collision_idx = i;
            this->game->powerup_id = p[0].randomPowerUp(this->game);
        }
    }
    #pragma GCC diagnostic ignored "-Wsign-compare"
    if (collision_idx == -1) {
        return;
    }
    for (long unsigned int i = 0; i < this->game->powerup_vec.size(); i++) {
        if (collision_idx != i) {
            p_new.push_back(p[i]);
        }
    }
    this->game->powerup_vec = p_new;
}

Snake::Snake(Game* g, int id, int x, int y, std::pair<std::string, std::string> textures)
{
    this->game = g;
    this->head_texture.loadFromFile(textures.first);
    this->texture.loadFromFile(textures.second);
    this->sprite.setTexture(this->texture);
    this->head_sprite.setTexture(this->head_texture);
    this->dir = Snake::Direction::Right;
    this->snake_queue = new std::deque<Point>();
    this->snake_queue->push_back(Point(x, y));
    this->id = id;
}

Snake::~Snake() { }
