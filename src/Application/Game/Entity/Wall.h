#ifndef WALL_H_
#define WALL_H_
#include "Entity.h"
#include <SFML/Graphics.hpp>

class WallEntity : public Entity {
public:
    sf::Texture texture;
    sf::Sprite sprite;
    float x;
    float y;

    WallEntity();
    virtual ~WallEntity();
};
#endif // ENTITY_H_