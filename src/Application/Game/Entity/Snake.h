#ifndef SNAKE_H
#define SNAKE_H

#include "Entity.h"
#include "Food.h"
#include "../Point.h"
#include "Powerup.h"
#include <SFML/Graphics.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/System/Vector2.hpp>
#include <queue>
#include <string>
#include <vector>
class Game;


class Snake : public Entity {
public:
    std::pair<std::string, std::string> textures;
    sf::Texture head_texture;
    sf::Sprite head_sprite;
    Game* game;
    enum Direction { Up, Right, Down, Left };
    Direction dir;
    Direction last_move;
    std::deque<Point> *snake_queue;
    int id;
    void spawn(float x, float y);
    void move();
    bool canMove();
    void maybeGrow();
    void consumeThyPowerup();
    Snake(Game*, int, int, int, std::pair<std::string,std::string>);
    ~Snake();
};

#endif
