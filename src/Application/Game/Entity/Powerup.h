#ifndef POWERUP_H_
#define POWERUP_H_

#include "Entity.h"
#include <SFML/Graphics.hpp>
class Game;
enum Powerups {
    SpeedUp,
    SlowDown,
    ExtraFoods,
    SuperFood
};

class Powerup : public Entity {
public:
    void speedUp(Game*);
    void slowDown(Game*);
    void superFood(Game*);
    void extraFoods(Game*);
    int randomPowerUp(Game*);
    Powerup();
    ~Powerup();
};

#endif
