#include "Board.h"
#include <cstddef>
#include <vector>

Board::Board(int w, int h, TileType init_type) {
    this->board = std::vector<std::vector<TileType>>(h, std::vector<TileType>(w, init_type));
}
Board::~Board() {}
