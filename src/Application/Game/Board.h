#ifndef BOARD_H
#define BOARD_H

#include <vector>
enum TileType {
    Empty,
    Wall
};

class Board {
 public:
    std::vector<std::vector<TileType>> board;
    Board(int, int, TileType);
    ~Board();
};

#endif
