#include "Game.h"
#include "../Application.h"
#include "../Renderer/Button.h"
#include "../StateManager.h"
#include "Board.h"
#include "Entity/Entity.h"
#include "Entity/Food.h"
#include "Entity/Powerup.h"
#include "Entity/Snake.h"
#include "Entity/Tile.h"
#include "YamlReader.h"
#include <fnmatch.h>
#include <ftw.h>
#include <iostream>
#include <queue>
#include <random>
#include <string>
#include <vector>

std::vector<std::string> v;

#pragma GCC diagnostic ignored "-Wunused-parameter"
static int add_from_walk(const char* fpath,
    const struct stat* sb,
    int typeflag)
{
    if (typeflag == FTW_F)
        if (fnmatch("*.yaml", fpath, FNM_CASEFOLD) == 0)
            v.push_back(fpath);
    return 0;
}

Game::Game(Application* app)
    : board(0, 0, Wall)
{
    ftw("boards", add_from_walk, 8);
    this->boards = v;
    this->application = app;
    this->board = this->yaml_reader.readBoard("boards/default-board.yaml");
    this->clock = sf::Clock();
}

Game::~Game() { }

void Game::updateKnownBoards()
{
    v.clear();
    ftw("boards", add_from_walk, 8);
    this->boards = v;
}

void Game::manageKey(sf::Keyboard::Key key)
{
    using Direction = Snake::Direction;
    switch (key) {
    case sf::Keyboard::W:
        if (this->players[0]->last_move == Direction::Down)
            return;
        this->players[0]->dir = Direction::Up;
        break;
    case sf::Keyboard::A:
        if (this->players[0]->last_move == Direction::Right)
            return;
        this->players[0]->dir = Direction::Left;
        break;
    case sf::Keyboard::S:
        if (this->players[0]->last_move == Direction::Up)
            return;
        this->players[0]->dir = Direction::Down;
        break;
    case sf::Keyboard::D:
        if (this->players[0]->last_move == Direction::Left)
            return;
        this->players[0]->dir = Direction::Right;
        break;

    case sf::Keyboard::Up:
        if (this->players[1]->last_move == Direction::Down)
            return;
        this->players[1]->dir = Direction::Up;
        break;
    case sf::Keyboard::Left:
        if (this->players[1]->last_move == Direction::Right)
            return;
        this->players[1]->dir = Direction::Left;
        break;
    case sf::Keyboard::Down:
        if (this->players[1]->last_move == Direction::Up)
            return;
        this->players[1]->dir = Direction::Down;
        break;
    case sf::Keyboard::Right:
        if (this->players[1]->last_move == Direction::Left)
            return;
        this->players[1]->dir = Direction::Right;
        break;
    case sf::Keyboard::Key::Escape:
        this->application->state_manager->pushState(State::PauseMenu);
        this->application->audio_player->pauseMusic(GameMusic);
    default:
        return;
    }
}

// set up the default game conditions. Spawn food and powerups
void Game::init()
{
    this->food_vec.clear();
    this->powerup_vec.clear();
    this->players.clear();
    this->clock.restart();
    this->powerup_counter = { 0, 0, 0, 0 };
    this->powerup_modifiers = { 0, 0, 0, 0 };
    this->powerup_id = -1;
    this->lose_state = LoseState::None;

    this->players = {
        new Snake(this, 1, 1, 1, { "img/player1_head.png", "img/player1.png" }),
        new Snake(this, 2, 1, 4, { "img/player2_head.png", "img/player2.png" })
    };

    this->maybeSpawnFood();
    this->maybeSpawnPowerUp();
    players[0]->spawn(0 + PADDING, 0 + PADDING);
    players[1]->spawn(0 + PADDING, 10 + PADDING);

    if (this->single_player) {
        this->players.pop_back();
    }
}

void Game::loop()
{
    while (!(this->clock.getElapsedTime().asMilliseconds() < MILIS_BETWEEN_UPDATES - powerup_modifiers[SpeedUp] + powerup_modifiers[SlowDown])) {
        this->clock.restart();
        this->lose_state = 0;
        for (auto& player : this->players) {
            if (player->canMove())
                player->move();
            else
                this->lose_state += player->id;
        }
        if (this->lose_state != LoseState::None) {
            this->application->state_manager->pushState(EndGame);
            this->application->audio_player->stopMusic(GameMusic);
            this->application->audio_player->end_game.stop();
            this->application->audio_player->playMusic(EndGameMusic);
        }
        this->maybeSpawnFood();
        this->maybeSpawnPowerUp();
        this->maybePowerupReset();
        this->powerupCountDown();
    }
}

int Game::getFoodCount()
{
    return this->food_vec.size();
}

int Game::getPowerupCount()
{
    return this->powerup_vec.size();
}

void Game::maybeSpawnFood()
{
    int fn = this->getFoodCount();
    int deficit = TARGET_FOOD + powerup_modifiers[ExtraFoods] - fn;
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<> rand_x(0, (HORIZONTAL_TILES - 2)); // define the range
    std::uniform_int_distribution<> rand_y(0, (VERTICAL_TILES - 2)); // define the range
    while (deficit-- > 0) {
        Food* f = new Food();
    roll:
        int x, y;
        x = rand_x(gen);
        y = rand_y(gen);
        if (this->board.board.at(y).at(x) == TileType::Wall)
            goto roll;
        f->x = x * SPRITE_SIZE + PADDING;
        f->y = y * SPRITE_SIZE + PADDING;
        this->food_vec.push_back(*f);
    }
}

void Game::maybeSpawnPowerUp()
{
    int pn = this->getPowerupCount();
    if (pn == TARGET_POWERUP)
        return;
    int deficit = TARGET_POWERUP - pn;
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<> rand_x(0, (HORIZONTAL_TILES - 2)); // define the range
    std::uniform_int_distribution<> rand_y(0, (VERTICAL_TILES - 2)); // define the range
    while (deficit-- > 0) {
        Powerup* p = new Powerup();
    roll:
        int x, y;
        x = rand_x(gen);
        y = rand_y(gen);
        if (this->board.board.at(y).at(x) == TileType::Wall)
            goto roll;
        p->x = x * SPRITE_SIZE + PADDING;
        p->y = y * SPRITE_SIZE + PADDING;
        this->powerup_vec.push_back(*p);
    }
}

void Game::render()
{
    Tile tile;
    Powerup powerup;
    WallEntity wall_entity;
    std::vector<Entity> hud_elements(2);
    std::vector<sf::Text> hud_text_elements(4);

    this->application->windowP->clear(sf::Color(40, 40, 40, 255));
    int tile_switcher = 0;
    for (int i = 0; i < HORIZONTAL_TILES; i++) {
        for (int j = 0; j < VERTICAL_TILES; j++) {
            sf::RectangleShape tile;
            tile.setPosition(i * SPRITE_SIZE + PADDING, j * SPRITE_SIZE + PADDING);
            tile.setSize(sf::Vector2f(SPRITE_SIZE, SPRITE_SIZE));
            if (tile_switcher % 2 == 0)
                tile.setFillColor(sf::Color(40, 40, 40));
            else
                tile.setFillColor(sf::Color(44, 44, 44));
            tile_switcher++;
            this->application->windowP->draw(tile);
        }
    }

    for (unsigned long int row = 0; row < this->application->game->board.board.size(); row++) {
        for (unsigned long int col = 0; col < this->application->game->board.board[row].size(); col++) {
            if (this->application->game->board.board[row][col] == TileType::Wall) {
                wall_entity.sprite.setPosition(col * SPRITE_SIZE + PADDING, row * SPRITE_SIZE + PADDING);
                this->application->windowP->draw(wall_entity.sprite);
            }
        }
    }

    for (auto& p : this->application->game->powerup_vec) {
        p.sprite.setPosition(p.x, p.y);
        this->application->windowP->draw(p.sprite);
    }
    for (auto& f : this->application->game->food_vec) {
        f.sprite.setPosition(f.x, f.y);
        this->application->windowP->draw(f.sprite);
    }

    // convert a queue into a vector
    for (auto& snake : this->application->game->players) {
        std::vector<Point> v(snake->snake_queue->begin(), snake->snake_queue->end());
        for (long unsigned int i = 0; i < snake->snake_queue->size(); i++) {
            snake->sprite.setPosition(
                v[i].x * SPRITE_SIZE + PADDING,
                v[i].y * SPRITE_SIZE + PADDING);
            this->application->windowP->draw(snake->sprite);
        }
        snake->head_sprite.setOrigin(8, 8);
        snake->head_sprite.setPosition(
            snake->snake_queue->back().x * SPRITE_SIZE + PADDING + 8,
            snake->snake_queue->back().y * SPRITE_SIZE + PADDING + 8);
        snake->head_sprite.setRotation(snake->dir * 90);
        this->application->windowP->draw(snake->head_sprite);
    }

    // HUD
    hud_elements[0].texture.loadFromFile("img/player1_head.png");
    hud_elements[0].sprite.setTexture(hud_elements[0].texture);
    hud_elements[1].texture.loadFromFile("img/player2_head.png");
    hud_elements[1].sprite.setTexture(hud_elements[1].texture);

    for (int i = 0; i < 2; i++) {
        if (this->single_player && i == 1)
            break;

        hud_elements[i].sprite.setScale(2.0f, 2.0f);
        hud_elements[i].sprite.setPosition(i * 150 + 60, 770);
        hud_elements[i].sprite.setRotation(180.0f);
        hud_text_elements[i].setFont(this->application->font);
        hud_text_elements[i].setCharacterSize(32);
        hud_text_elements[i].setFillColor(TEXT_COLOR);
        hud_text_elements[i].setString(":" + std::to_string(this->players[i]->snake_queue->size()));
        hud_text_elements[i].setPosition(i * 150 + 65, 732);

        this->application->windowP->draw(hud_elements[i].sprite);
        this->application->windowP->draw(hud_text_elements[i]);
    }
    hud_text_elements[2].setFont(this->application->font);
    hud_text_elements[2].setCharacterSize(32);
    hud_text_elements[2].setFillColor(TEXT_COLOR);
    hud_text_elements[2].setString("LAST POWER UP:");
    hud_text_elements[2].setPosition(600, 732);
    hud_text_elements[3].setFont(this->application->font);
    hud_text_elements[3].setCharacterSize(32);
    hud_text_elements[3].setFillColor(TEXT_COLOR);
    hud_text_elements[3].setString(getLatestPowerup(this->powerup_id));
    hud_text_elements[3].setPosition(840, 732);
    this->application->windowP->draw(hud_text_elements[2]);
    this->application->windowP->draw(hud_text_elements[3]);
}

// comments left for debug purpouses
void Game::powerupCountDown()
{
    for (auto& powerup : this->powerup_counter) {
        if (powerup > 0)
            powerup--;
        // std::cerr<<powerup<<" ";
    }
    // std::cerr<<std::endl;
}

void Game::maybePowerupReset()
{
    if (this->powerup_counter[SpeedUp] == 0)
        this->powerup_modifiers[SpeedUp] = 0;
    else if (this->powerup_counter[SlowDown] == 0)
        this->powerup_modifiers[SlowDown] = 0;
    else if (this->powerup_counter[ExtraFoods] == 0)
        this->powerup_modifiers[ExtraFoods] = 0;
}

std::string Game::getLatestPowerup(int powerup_id)
{
    switch (powerup_id) {
    case SpeedUp:
        return "Speed Up";
        break;
    case SlowDown:
        return "Slow Down";
        break;
    case ExtraFoods:
        return "Extra Food";
        break;
    case SuperFood:
        return "Super Food";
        break;
    default:
        return "None";
        break;
    }
}
